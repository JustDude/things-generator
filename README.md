# Things Generator

- docker network create -d bridge ThingsGenerator
- docker-compose up --build --force-recreate -d

___________________________________________________________

To build flask (backend) locally:
(need to have python and pip installed locally in the machine)
check Versions:
- python3 --version
- flask --version
- pip --version

cd into mainflask
run this commands:
- cd things-generator
- cd mainflask
- pip install --upgrade pip
- pip install -r requirements.txt
- export FLASK_ENV=development
- flask run

this should run flask in debug mode
