from app import app
from ..models.UserModel import User


@app.route('/')
def home():
    testUser = User()
    testUser.maledetto = "SUCA"

    return testUser.__dict__

@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    header['Access-Control-Allow-Methods'] = 'OPTIONS, HEAD, GET, POST, DELETE, PUT'
    return response
